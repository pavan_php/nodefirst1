var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET user details. */
router.get('/api/userDetails', function(req, res, next) {
  res.json({ username: 'Flavio' })
});

module.exports = router;
